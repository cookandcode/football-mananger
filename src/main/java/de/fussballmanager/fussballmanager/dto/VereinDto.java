package de.fussballmanager.fussballmanager.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;



public class VereinDto {
	
	@NotEmpty
	private String vereinsName;
	@Min(1850)
	private int foundedAt;
	
	
	public String getVereinsName() {
		return vereinsName;
	}
	public void setVereinsName(String vereinsName) {
		this.vereinsName = vereinsName;
	}
	public int getFoundedAt() {
		return foundedAt;
	}
	public void setFoundedAt(int foundedAt) {
		this.foundedAt = foundedAt;
	}
	
	

}
