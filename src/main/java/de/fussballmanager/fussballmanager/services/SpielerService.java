package de.fussballmanager.fussballmanager.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fussballmanager.fussballmanager.dto.SpielerDto;
import de.fussballmanager.fussballmanager.entites.Spieler;
import de.fussballmanager.fussballmanager.entites.Verein;
import de.fussballmanager.fussballmanager.repositories.SpielerRepository;

@Service
public class SpielerService {
	
	private SpielerRepository spielerRepository;
	
	@Autowired
	public SpielerService(SpielerRepository spielerRepository) {
		this.spielerRepository = spielerRepository;
	}

	public List<Spieler> getSpieler() {
		return spielerRepository.findAll();
	}
	
	public Spieler getOne(int id) {
		return spielerRepository.getOne(id);
	}


	public void save(Spieler spieler) {
		spielerRepository.save(spieler);
		
	}
	
	
	public void update(@Valid SpielerDto spielerDto, int id) {
		Spieler currentSpieler = spielerRepository.getOne(id);
		currentSpieler.setName(spielerDto.getName());
		spielerRepository.save(currentSpieler);
	}

	public void delete(int id) {
		spielerRepository.delete(spielerRepository.getOne(id));
		
	}
	
	

}
