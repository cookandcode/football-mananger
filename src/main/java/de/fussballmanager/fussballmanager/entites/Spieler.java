package de.fussballmanager.fussballmanager.entites;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class Spieler {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull(message = "is required")
	@Size(min = 1, message = "is required")
	private String name;
	
	private int nummer;
	
	private long gehalt;
	
	@ManyToOne
	private Verein verein;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public long getGehalt() {
		return gehalt;
	}

	public void setGehalt(long gehalt) {
		this.gehalt = gehalt;
	}

	public Verein getVerein() {
		return verein;
	}

	public void setVerein(Verein verein) {
		this.verein = verein;
	}

	public int getId() {
		return id;
	}
	
	
	
	
	
	
}
