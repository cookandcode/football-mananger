package de.fussballmanager.fussballmanager.entites;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Verein {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull(message = "is required")
	@Size(min = 1, message = "is required")
	private String vereinsName;
	private int foundedAt;
	
	@OneToMany(mappedBy = "verein", cascade = CascadeType.ALL)
	private List<Spieler> spielerListe;

	public String getVereinsName() {
		return vereinsName;
	}

	public void setVereinsName(String vereinsName) {
		this.vereinsName = vereinsName;
	}

	public List<Spieler> getSpielerListe() {
		return spielerListe;
	}


	public void setSpielerListe(List<Spieler> spielerListe) {
		this.spielerListe = spielerListe;
	}

	public int getId() {
		return id;
	}

	public int getFoundedAt() {
		return foundedAt;
	}

	public void setFoundedAt(int foundedAt) {
		this.foundedAt = foundedAt;
	}
	
	
	
	
	

}
